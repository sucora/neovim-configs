vim.g.mapleader = ","

local opt = vim.opt

opt.autoindent = true
opt.autoread = true
opt.clipboard = "unnamedplus" -- sync with system clipboard
opt.confirm = true
opt.cursorline = true
opt.expandtab = true
opt.number = true
opt.shiftwidth = 4
opt.softtabstop = 4
opt.tabstop = 4
opt.undofile = true
