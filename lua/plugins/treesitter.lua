return {
  "nvim-treesitter/nvim-treesitter",
  build = ":TSUpdate",

  opts = {
    ensure_installed = {
      "rust",
      "typescript",
      "javascript",
      "lua",
      "vim",
      "vimdoc",
    },
    sync_install = false,
    auto_install = true,
    highlight = {
      enable = true,
      additional_vim_regex_highlighting = true
    },
    indent = { enable = true }
  },

  -- pass opts to nvim-treesitter.configs instead of MAIN
  config = function(_, opts)
    require("nvim-treesitter.configs").setup(opts)
  end
}
