return {
  "nvim-telescope/telescope.nvim",
  tag = "0.1.2",
  dependencies = { "nvim-lua/plenary.nvim", "nvim-tree/nvim-web-devicons" },
  opts = {
    extensions = {
      file_browser = { theme = "ivy", hijack_netrw = true }
    }
  },
  init = function()
    require("telescope").load_extension("file_browser")
    local set = vim.keymap.set

    set("n", "<leader>ff", require("telescope").extensions.file_browser.file_browser)
  end
}
