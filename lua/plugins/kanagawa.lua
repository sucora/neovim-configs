return {
  "rebelot/kanagawa.nvim",
  lazy = true,

  opts = {
    keywordStyle = { italic = false },
    statementStyle = { bold = false },
    transparent = true,
    theme = "wave",
    colors = {
      theme = { all = { ui = { bg_gutter = "none" } } } -- remove background of LineNr
    },

    overrides = function(colors)
      local theme = colors.theme

      return {
        NormalFloat = { bg = "none" },
        FloatBorder = { bg = "none" },
        FloatTitle = { bg = "none" },

        NormalDark = { bg = theme.ui.bg_m3, fg = theme.ui.fg_dim },
        LazyNormal = { bg = theme.ui.bg_m3, fg = theme.ui.fg_dim },

        TelescopeNormal = { bg = "none" },
        TelescopeTitle = { fg = theme.ui.special, bold = true },
        TelescopeBorder = { bg = "none", fg = theme.ui.bg_m3 },

        Pmenu = { bg = theme.ui.bg_p1, fg = theme.ui.shade0 },
        PmenuSel = { bg = theme.ui.bg_p2, fg = "none" },
        PmenuSbar = { bg = theme.ui.bg_m1 },
        PmenuThumb = { bg = theme.ui.bg_p2 },

        Boolean = { bold = false }
      }
    end
  }
}
